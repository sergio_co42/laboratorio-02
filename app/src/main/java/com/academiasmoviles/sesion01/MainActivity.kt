package com.academiasmoviles.sesion01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //Evento del boton verificar
        btnVerificar.setOnClickListener {
            val nacimiento = edtNacimiento.text.toString().toInt()

            when (nacimiento) {
                in 1930 .. 1948 -> {
                    tvResultado.text = "Silent Generation"
                    tvPoblacion.text = "6.300.000"
                    iVimagen.setImageResource(R.drawable.austeridad)
                }
                in 1949 .. 1968 -> {
                    tvResultado.text = "Baby Boom"
                    tvPoblacion.text = "12.200.000"
                    iVimagen.setImageResource(R.drawable.ambicion)
                }
                in 1969 .. 1980 -> {
                    tvResultado.text = "Generacion X"
                    tvPoblacion.text = "9.300.000"
                    iVimagen.setImageResource(R.drawable.obsesion)
                }
                in 1981 .. 1993 -> {
                    tvResultado.text = "Generacion Y"
                    tvPoblacion.text = "7.200.000"
                    iVimagen.setImageResource(R.drawable.frustracion)
                }
                in 1994 .. 2010 -> {
                    tvResultado.text = "Generacion Z"
                    tvPoblacion.text = "7.800.000"
                    iVimagen.setImageResource(R.drawable.irreverencia)
                }
                else -> {
                    tvResultado.text = "No perteneces a ninguna generacion"
                    tvPoblacion.text = "Sin datos"
                    iVimagen.setImageResource(R.drawable.ic_launcher_background)
                }
            }
        }
    }
}